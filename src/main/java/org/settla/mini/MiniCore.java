package org.settla.mini;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.settla.mini.managers.team.MyTeam;
import org.settla.mini.managers.team.TeamManager;
import org.settla.mini.scoreboard.MyScoreboard;
import org.settla.mini.scoreboard.rows.Row;

/**
 * Created by dennisheckmann on 14.11.16.
 */
public class MiniCore extends JavaPlugin {

    private static MiniCore instance;

    public void onEnable(){
        instance = this;
        //Instantiate a new class extending Game here!
    }

    public static MiniCore instance(){
        return instance;
    }

}
