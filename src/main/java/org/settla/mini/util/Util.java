package org.settla.mini.util;

import org.bukkit.ChatColor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.settla.mini.MiniCore;

import java.io.*;

/**
 * Created by dennisheckmann on 12.11.16.
 */
public class Util {

    public static String uuid(Player p){
        return p.getUniqueId().toString();
    }

    public static String uuid(HumanEntity he){
        return he.getUniqueId().toString();
    }

    public static String translate(String text){
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    public static void copyResource(String fileName, File output){
        output.getParentFile().mkdirs();
        if(!output.exists()){
            BufferedReader br = new BufferedReader(new InputStreamReader(MiniCore.instance().getResource(fileName)));
            StringBuilder sb = new StringBuilder();
            String temp = null;
            try {
                while((temp = br.readLine()) != null){
                    sb.append(temp  + "\n");
                }
                FileWriter fw = new FileWriter(output);
                fw.write(sb.toString());
                fw.flush();
                fw.close();
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Vector rotateAroundAxisX(Vector v, double angle) {
        angle = Math.toRadians(angle);
        double y, z, cos, sin;
        cos = Math.cos(angle);
        sin = Math.sin(angle);
        y = v.getY() * cos - v.getZ() * sin;
        z = v.getY() * sin + v.getZ() * cos;
        return v.setY(y).setZ(z);
    }

    public static Vector rotateAroundAxisY(Vector v, double angle) {
        angle = -angle;
        angle = Math.toRadians(angle);
        double x, z, cos, sin;
        cos = Math.cos(angle);
        sin = Math.sin(angle);
        x = v.getX() * cos + v.getZ() * sin;
        z = v.getX() * -sin + v.getZ() * cos;
        return v.setX(x).setZ(z);
    }

    public static Vector rotateAroundAxisZ(Vector v, double angle) {
        angle = Math.toRadians(angle);
        double x, y, cos, sin;
        cos = Math.cos(angle);
        sin = Math.sin(angle);
        x = v.getX() * cos - v.getY() * sin;
        y = v.getX() * sin + v.getY() * cos;
        return v.setX(x).setY(y);
    }

}
