package org.settla.mini.util.item;

import org.bukkit.inventory.ItemStack;
import org.settla.mini.MiniCore;
import org.settla.mini.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ItemManager {
	
	private final List<MyItem> items;

	public ItemManager(){
		items = new ArrayList<>();
		initItems();
	}
	
	public List<MyItem> getMyItems(){
		return new ArrayList<>(items);
	}
	
	public List<ItemStack> getItems(){
		List<ItemStack> copy = new ArrayList<>(items.size());
		for(MyItem item : items){
		    copy.add(item.getItem());
        }
		return copy;
	}
	
	public void registerItem(String name, ItemStack item){
		items.add(new MyItem(name, item));
	}
	
	public ItemStack getItem(String name){
		for(MyItem item : items){
			if(item.name.equalsIgnoreCase(name)){
				return item.item.clone();
			}
		}
		return null;
	}
	
	private void initItems(){
		copyPremadeFiles();
		File itemDir = getItemDir();
		itemDir.mkdirs();
		File[] files = itemDir.listFiles();
		for(File f : files){
			List<ItemFactory.ItemData> datas = ItemFactory.getItemsFrom(f);
			if(datas == null)
				continue;
			for(ItemFactory.ItemData data : datas){
				ItemStack temp = getItem(data.getId());
				if(temp != null){
					System.out.println("An item with the name " + data.getId() + " was registered twice! Will only use the first one.");
				} else {
					items.add(new MyItem(data.getId(), data.getItem()));
				}
			}
		}
		System.out.println("Successfully registered " + items.size() + " items!");
	}
	
	private void copyPremadeFiles(){
		File dir = getItemDir();
		Util.copyResource("items.xml", new File(dir, "items.xml"));
	}
	
	public File getItemDir() {
		return new File(MiniCore.instance().getDataFolder(), "items");
	}

    public static class MyItem {
		
		private String name;
		private ItemStack item;
		
		public MyItem(String name, ItemStack item) {
			this.name = name;
			this.item = item;
		}
		
		public boolean isData(String name){
			return this.name.equalsIgnoreCase(name);
		}
		
		public ItemStack getItem(){
			return item.clone();
		}
		
	}
	
}
