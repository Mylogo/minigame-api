package org.settla.mini.util;

/**
 * Created by dennisheckmann on 14.11.16.
 */
@FunctionalInterface
public interface Returner<T> {

    public T create();

}
