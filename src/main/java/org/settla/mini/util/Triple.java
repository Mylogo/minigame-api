package org.settla.mini.util;

public class Triple<T, E, V> extends Tuple<T, E> {

	private V third;
	
	public Triple(T first, E second, V third) {
		super(first, second);
		this.third = third;
	}
	
	public V getThird(){
		return third;
	}
	
	public void setThird(V third){
		this.third = third;
	}

}
