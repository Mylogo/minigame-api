package org.settla.mini.util;

import org.bukkit.scheduler.BukkitRunnable;

public abstract class CancellableRunnable extends BukkitRunnable {

	private boolean cancelled;
	
	@Override
	public final void run() {
		if (cancelled) {
			onCancel();
			this.cancel();
		} else {
			onRun();
		}
	}
	
	/**
	 * will be executed after the delay and optional period set
	 * in runTaskTimer*. It's the replacement of the standard
	 * {@link #run()} because run contains default methods.
	 */
	public abstract void onRun();
	
	/**
	 * will be executed after {@link #cancelMe()} when {@link #onRun()}
	 * was prevented 
	 */
	public void onCancel(){}
	
	/**
	 * Firing this method sets {@link #cancelled} to true. Therefore
	 * the next time {@link #run()) runs, the scheduler will
	 * be cancelled and {@link #onRun()} won't run
	 */
	public final void cancelMe(){
		cancelled = true;
	}
	
}
