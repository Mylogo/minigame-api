package org.settla.mini.game.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.settla.mini.game.GameState;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dennisheckmann on 15.11.16.
 * This is just an example on how to extend from {@link MyListener}
 */
public class PlayerJoinListener extends MyListener<PlayerJoinEvent> {

    @EventHandler
    public void onEvent(PlayerJoinEvent playerJoinEvent) {
        super.onEvent(playerJoinEvent);
    }

    @Override
    public List<SubPerformer> initPerformers() {
        return Arrays.asList(
                new AlwaysPerformer() {
                    @Override
                    public void perform(PlayerJoinEvent t) {
                        System.out.println("I am always being performed on join!");
                    }
                },
                new SubPerformer(GameState.LOBBY) {
                    @Override
                    public void perform(PlayerJoinEvent e) {
                        System.out.println("Joined! " + e.getPlayer().getName() + " in Lobby phase");
                    }
                }
        );
    }

}
