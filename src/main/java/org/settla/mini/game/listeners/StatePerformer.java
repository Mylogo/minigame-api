package org.settla.mini.game.listeners;

import org.settla.mini.game.GameState;
import sun.misc.Perf;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by dennisheckmann on 15.11.16.
 */
public abstract class StatePerformer<PerformOn, Condition> {

    private List<SubPerformer> performers;

    public StatePerformer() {
        performers = new CopyOnWriteArrayList<>(initPerformers());
    }

    public List<SubPerformer> getPerformers() {
        System.out.println("Having:" + performers.size());
        return performers;
    }

    public abstract List<SubPerformer> initPerformers();

//    @FunctionalInterface
    public abstract class SubPerformer {
        private Condition condition;
        public SubPerformer(Condition condition){
            this.condition = condition;
        }
        public abstract void perform(PerformOn t);
        public boolean performs(Condition o) {
            System.out.println("Checking: " +  o + " and" + condition);
            return this.condition.equals(o);
        }
    }

    public abstract class AlwaysPerformer extends SubPerformer {
        public AlwaysPerformer() {
            super(null);
        }

        @Override
        public boolean performs(Condition o) {
            return true;
        }
    }


}