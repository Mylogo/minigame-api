package org.settla.mini.game.listeners;

import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.settla.mini.game.Game;
import org.settla.mini.game.GameState;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dennisheckmann on 15.11.16.
 */
public abstract class MyListener<E extends Event> extends StatePerformer<E, GameState> implements Listener {

    public MyListener() {
        super();
    }

    public void onEvent(E e) {
        GameState current = Game.instance().getState();
//        getPerformers().stream().filter(performer -> performer.performs(current)).findAny().ifPresent(performer -> performer.perform(e));
        for (SubPerformer performer : getPerformers()) {
            System.out.println("checking now!");
            if (performer.performs(current))
                performer.perform(e);
        }
    }

}
