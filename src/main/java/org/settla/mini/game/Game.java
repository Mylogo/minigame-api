package org.settla.mini.game;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.settla.mini.MiniCore;
import org.settla.mini.game.states.StateRunnable;
import org.settla.mini.managers.Manager;
import org.settla.mini.util.Returner;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by dennisheckmann on 14.11.16.
 */
public abstract class Game {

    private List<Manager> managers;
    private List<StateRunnable> states;
    protected MiniCore plugin;
    private GameState currentState;
    private StateRunnable currentRunnable;
    private int currentIndex;

    public Game(MiniCore instance) {
        if(instance == null)
            throw new NullPointerException("MiniCore instance must not be null");
        plugin = instance;
        managers = new CopyOnWriteArrayList<>();
        states = new CopyOnWriteArrayList<>(); //probably irrelevant to have concurrency safety
        startInit();
        getStates().forEach(sr -> states.add(sr.create()));
        if(states.size() == 0)
            throw new IllegalArgumentException("Must always return Consumers! >:( ");
        currentIndex = 0; //ugly as fuck
        currentRunnable = states.get(currentIndex);
        currentState = currentRunnable.getState();
        startCurrentRunnable();
    }

    protected void addManager(Manager m) {
        managers.add(m);
    }

    /** @return whether the push was successful/not the last one */
    public boolean pushRunnable() {
        int newIndex = currentIndex + 1;
        if(newIndex >= states.size()) {
            return false;
        } else {
            currentRunnable.cancelMe();
            currentRunnable = states.get(newIndex);
            currentState = currentRunnable.getState();
            currentIndex = newIndex;
            return true;
        }
    }

    public void startCurrentRunnable() {
        currentRunnable.onStateStart();
        currentRunnable.runTaskTimer(plugin, 0, 0);
    }

    protected abstract Listener[] getListeners();

    public GameState getState(){
        return currentState;
    }

    public StateRunnable getCurrentRunnable() {
        return currentRunnable;
    }

    @Deprecated
    public void setState(GameState state) {
        currentState = state;
        onStateChange(currentState);
        switch(currentState) {
            case LOBBY:
                startLobby();
                break;
            case GAME:
                startGame();
                break;
            case DEATH_MATCH:
                startDeathMatch();
                break;
            case GG:
                startGG();
                break;
            default:
                throw new IllegalArgumentException("Wtf did you give me..?");
        }
    }

    private void startGG() {
        onGGStart();
    }

    private void startDeathMatch() {
        onDeathMatchStart();
    }

    private void startGame() {
        onGameStart();
    }

    private void startLobby() {
        onLobbyStart();
    }

    /** Listeners from {@link #getListeners()} are being registered*/
    private void startInit() {
        for(Listener l : getListeners()){
            registerListener(l);
        }
        onInit();
    }

    public <T extends Manager> T getManager(Class<T> managerClazz) {
        for(Manager m : managers)
            if(managerClazz.isInstance(managerClazz))
                return managerClazz.cast(m);
        return null;
    }

    protected void registerListener(Listener l) {
        Bukkit.getPluginManager().registerEvents(l, plugin);
    }

    @Deprecated
    /** May be overridden. Doesn't do anything right now
     * and gets called at the end of {@link #getState()}
     * @param state the same as currentState */
    protected void onStateChange(GameState state) {}

    protected abstract void onInit();
    protected void onLobbyStart() {}
    protected void onLobbyEnd() {}
    protected void onGameStart() {}
    protected void onGameEnd() {}
    protected void onDeathMatchStart() {}
    protected void onDeathMatchEnd() {}
    protected void onGGStart() {}
    protected void onGGEnd() {}

    public abstract List<Returner<StateRunnable>> getStates();

}
