package org.settla.mini.game.states;

import org.settla.mini.game.Game;
import org.settla.mini.game.GameState;
import org.settla.mini.util.CancellableRunnable;

/**
 * Created by dennisheckmann on 14.11.16.
 */
public abstract class StateRunnable extends CancellableRunnable {

    private GameState myState;

    public abstract void onStateStart();
    public abstract void onStateEnd();

    public StateRunnable(GameState state){
        myState = state;
    }

    @Override
    public final void onCancel() {
        onStateEnd();
        Game.instance().pushRunnable();
    }

    public GameState getState() {
        return myState;
    }

}
