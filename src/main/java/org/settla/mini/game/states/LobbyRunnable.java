package org.settla.mini.game.states;

import org.settla.mini.game.GameState;

/**
 * Created by dennisheckmann on 15.11.16.
 */
public class LobbyRunnable extends StateRunnable {

    /** Depending on teams */
    private final int MIN_PLAYERS_REQUIRED = 2;

    public LobbyRunnable() {
        super(GameState.LOBBY);
    }

    @Override
    public void onStateStart() {
        System.out.println("Lobby started");
    }

    @Override
    public void onStateEnd() {

    }

    @Override
    public void onRun() {

    }

}
