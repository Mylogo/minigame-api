package org.settla.mini.game;

/**
 * Created by dennisheckmann on 14.11.16.
 */
public enum GameState {

    /** Choosing teams, maybe some after_init etc.*/
    LOBBY,

    /** When players play.. :O */
    GAME,

    /** Optional per MiniGame */
    DEATH_MATCH,

    /** Last Phase */
    GG


}
