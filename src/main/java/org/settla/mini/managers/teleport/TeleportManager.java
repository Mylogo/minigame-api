package org.settla.mini.managers.teleport;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.settla.mini.managers.Manager;
import org.settla.mini.managers.teleport.teleportation.Teleportation;
import org.settla.mini.util.ConfigManager;


public class TeleportManager extends Manager {

	public static final String NAME = "Teleport";
	private static TeleportManager instance;
	private Map<String, Teleportation> teleportations;

	@Override
	public void onEnable() {
		instance = this;
		super.onEnable();
		initConfig();
		teleportations = new HashMap<>();
	}

	private void initConfig() {
		FileConfiguration conf = ConfigManager.getConfig("Teleportation");
		conf.addDefault("messages.send_per_second", "Gonna be teleported in %seconds% seconds");
		conf.addDefault("messages.on_teleport", "You were teleported to %destination%");
		conf.addDefault("messages.on_teleport_cancelled", "Your teleport to %destination% was cancelled.");
		conf.options().copyDefaults(true);
		ConfigManager.saveConfig("Teleportation");
	}

	@Override
	public String getName() {
		return NAME;
	}

	public static TeleportManager instance() {
		return instance;
	}

	public void proceed(Teleportation t) {
		Map<String, Teleportation> sync = Collections.synchronizedMap(teleportations);
		Teleportation current;
		synchronized (sync) {
			current = getCurrentTeleportation(t.getPlayer());
		}
		if (current != null) {
			current.cancelTeleport();
		}
		putTeleportation(sync, t);
	}

	public synchronized Teleportation getCurrentTeleportation(Player p) {
		return teleportations.get(p.getUniqueId().toString());
	}

//	private void putTeleportation(Teleportation t) {
//		putTeleportation(t);
//	}
	
	private void putTeleportation(Map<String, Teleportation> sync, Teleportation t) {
		synchronized (sync) {
			sync.put(t.getPlayer().getUniqueId().toString(), t);
		}
	}

	public void unProceeded(Teleportation teleportation) {
		Map<String, Teleportation> sync = Collections.synchronizedMap(teleportations);
		synchronized(sync){
			sync.remove(teleportation.getPlayer().getUniqueId().toString());
		}
	}

}
