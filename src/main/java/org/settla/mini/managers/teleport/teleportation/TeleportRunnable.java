package org.settla.mini.managers.teleport.teleportation;

import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.settla.mini.util.CancellableRunnable;


public abstract class TeleportRunnable extends CancellableRunnable {
	
	protected Player player;
	private Vector firstPosition;
	private double health;
	
	public TeleportRunnable(Player player){
		this.player = player;
		firstPosition = player.getLocation().toVector();
		health = player.getHealth();
	}
	
	/**
	 * fires {@link #hasMoved(double)} with a parameter of 0.5
	 * @return whether player has moved more than 0.7 blocks
	 * (0.7*0.7 = 0.5)
	 */
	public boolean hasMoved(){
		return hasMoved(0.5);
	}
	
	/**
	 * @param distanceSquared the distance he can move squared
	 * @return whether the player has moved more than
	 * square root of distanceQuared blocks
	 */
	public boolean hasMoved(double distanceSquared){
		return player.getLocation().toVector().distanceSquared(firstPosition) > distanceSquared;
	}
	
	/**
	 * checks if the player lost health since beginning of teleport
	 * @return if the current health is less then beginning health
	 */
	public boolean hasLostHealth(){
		return player.getHealth() < health;
	}
	
	/**
	 * combines {@link #hasMoved()} and {@link #hasLostHealth()}
	 * @return if one of the above said methods returns true
	 */
	public boolean lostHealthOrHasMoved(){
		return hasMoved() || hasLostHealth();
	}
	
}
