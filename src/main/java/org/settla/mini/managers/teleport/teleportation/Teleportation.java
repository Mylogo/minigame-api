package org.settla.mini.managers.teleport.teleportation;

import java.util.Objects;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.settla.mini.managers.teleport.TeleportManager;
import org.settla.mini.util.ConfigManager;
import org.settla.mini.util.Util;


public abstract class Teleportation {

	protected final Player player;
	protected final Location destination;
	protected final String destinationName;
	protected boolean started;
	protected TeleportRunnable runnable;

	/**
	 * @param p player to be teleported
	 * @param destination where he should be teleported to
	 * @param destinationName the name which's gonna display after he/she gets geleported
	 */
	public Teleportation(Player p, Location destination, String destinationName) {
		this.player = p;
		this.destination = destination;
		this.destinationName = destinationName;
		started = false;
	}

	/**
	 * this runnable will be started after {@link #proceed} and therefore
	 * in {@link onProceed}. Must always return a non-null value
	 * @return
	 */
	public abstract TeleportRunnable initRunnable();
	
	/**
	 * Teleportation is registered at TeleportManager and the TeleportRunnable
	 * is being initialized from {@link #initRunnable()}. After that
	 * {@link #onProceed()} is being fired
	 */
	public final void proceed() {
		TeleportManager.instance().proceed(this);
		started = true;
		runnable = initRunnable();
		onProceed();
	}

	/**
	 * This method should be used to start/run the TeleportRunnable returned from {@link #initRunnable()}
	 */
	protected abstract void onProceed();
	
	/**
	 * Cancels current teleportation and fires {@link #onCancel()}
	 * @throws IllegalStateException if teleport has not proceeded/started yet.
	 */
	public final void cancelTeleport() {
		if (!started)
			throw new IllegalStateException(
					"Teleportation " + getClass().getSimpleName() + " could not be proceeded because it didn't start");
		Objects.requireNonNull(runnable);
		runnable.cancel();
		onCancel();
	}

	/**
	 * method will be fired after {@link #cancelTeleport()}
	 */
	protected void onCancel() {
		player.sendMessage(Util.translate(ConfigManager.getConfig("Teleportation")
				.getString("messages.on_teleport_cancelled").replace("%destination%", destinationName)));
	}

	/**
	 * 
	 * @return the player who should be teleported
	 */
	public Player getPlayer() {
		return player;
	}
	
	/**
	 * 
	 * @return the location the player should be teleported to
	 */
	public Location getDestination(){
		return destination;
	}
	
	/**
	 * teleports to the specified destination
	 */
	protected void teleport(){
		player.teleport(destination);
		unregisterMe();
	}
	
	protected void unregisterMe(){
		TeleportManager.instance().unProceeded(this);
	}

}
