package org.settla.mini.managers.teleport.teleportation;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.settla.mini.MiniCore;
import org.settla.mini.util.ConfigManager;
import org.settla.mini.util.Util;

public class CountedTeleportation extends Teleportation {

	private int count;

	public CountedTeleportation(Player p, Location destination, String destinationName, int seconds) {
		super(p, destination, destinationName);
		count = seconds;
	}

	@Override
	public TeleportRunnable initRunnable() {
		return new TeleportRunnable(player) {
			public void onRun() {
				if(lostHealthOrHasMoved()){
					CountedTeleportation.this.onCancel();
					this.cancel();
					unregisterMe();
					return;
				}
				if (count == 0) {
					teleport();
					player.sendMessage(Util.translate(ConfigManager.getConfig("Teleportation")
							.getString("messages.on_teleport").replace("%destination%", destinationName)));
					this.cancel();
				} else {
					player.sendMessage(Util.translate(ConfigManager.getConfig("Teleportation")
							.getString("messages.send_per_second").replace("%seconds%", "" + count)));
					count--;
				}
			}
		};
	}

	@Override
	protected void onProceed() {
		runnable.runTaskTimer(MiniCore.instance(), 0, 20);
	}

}
