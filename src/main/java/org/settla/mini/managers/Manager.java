package org.settla.mini.managers;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.settla.mini.MiniCore;

public abstract class Manager {

	public void onEnable(){}
	
	public void onDisable(){}
	
	public abstract String getName();
	
	public void registerListener(Listener l){
		Bukkit.getPluginManager().registerEvents(l, MiniCore.instance());
	}
	
}
