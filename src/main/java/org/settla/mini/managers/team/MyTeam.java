package org.settla.mini.managers.team;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by dennisheckmann on 14.11.16.
 */
public class MyTeam {

    private List<Player> members;
    private final String teamName;
    private final Team team;

    protected MyTeam(String teamName) {
        this.teamName = teamName;
        members = new CopyOnWriteArrayList<>();
        team = getOrCreateTeam(this.teamName);
    }

    public void addPlayer(Player p) {
        members.add(p);
        team.addEntry(p.getName());
    }

    public void removePlayer(Player p) {
        members.remove(p);
        team.removeEntry(p.getName());
    }

    private static Team getOrCreateTeam(String teamName){
        Scoreboard sb = Bukkit.getScoreboardManager().getMainScoreboard();
        Team team = sb.getTeam(teamName);
        return team != null ? team : sb.registerNewTeam(teamName);
    }

//    public void addMember(Player p) {
//        members.add(p);
//    }

    public boolean isTeamName(String teamName) {
        return this.teamName.equalsIgnoreCase(teamName);
    }

    public boolean hasPlayer(Player p) {
        return members.contains(p);
    }

    public void setPrefix(String prefix) {
        team.setPrefix(prefix);
    }

    public void setAllowFriendlyFire(boolean friendlyFire) {
        team.setAllowFriendlyFire(friendlyFire);
    }

    public void setDisplayName(String displayName) {
        team.setDisplayName(displayName);
    }

    public void setColorPrefix(ChatColor color) {
        setPrefix("" + color);
    }

    public Team getTeam() {
        return team;
    }

}
