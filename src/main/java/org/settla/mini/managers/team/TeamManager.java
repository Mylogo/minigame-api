package org.settla.mini.managers.team;

import org.bukkit.entity.Player;
import org.settla.mini.managers.Manager;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by dennisheckmann on 14.11.16.
 */
public class TeamManager extends Manager {

    private List<MyTeam> teams;

    @Override
    public void onEnable() {
        teams = new CopyOnWriteArrayList<>();
    }

    public MyTeam getTeamOf(Player p) {
        for(MyTeam team : teams)
            if(team.hasPlayer(p))
                return team;
        return null;
    }

    public MyTeam getOrCreateTeam(String teamName) {
        MyTeam team = getTeam(teamName);
        if(team == null) {
            team = createTeam(teamName);
        }
        return team;
    }

    public MyTeam getTeam(String teamName) {
        return teams.stream().filter(team -> team.isTeamName(teamName)).findFirst().orElse(null);
    }

    public MyTeam createTeam(String teamName){
        MyTeam team = new MyTeam(teamName);
        teams.add(team);
        return team;
    }

    @Override
    public String getName() {
        return "Team";
    }
}
